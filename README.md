Dodržiavajte štandardné pravidlá z vnitrosemestráliek, obzvlášť:
- ak môže byť usporiadanie prirodzené, spravte ho prirodzené
- komparátory definujte ako anonymné triedy alebo iným spôsobom, ktorý nevyžaduje definíciu externej triedy
- existujúce triedy modifikujte iba ak je to nevyhnutné
- dodržiavajte best practices ohľadom používania final, static a pod.
- pre triedenie používajte zoradené kolekcie alebo streamy
- neskrývajte hodnoty v kóde, ale použite konštanty

<br />

K dispozícii máte triedu `Item` reprezentujúcu položku na sklade (napríklad džús).
* Upravte ju tak, aby vyhodila vhodnú výnimku, ak je pri vytváraní zadaná záporná alebo nulová cena.
* Zabezpečte, aby item po prevedení na String vrátil svoj názov a cenu (presný formát nie je predpísaný).

<br />

Ďalej máte k dispozícii triedu `Supplier` reprezentujúcu dodávateľa. Autor triedy Supplier odišiel z FI, pretože nezvládol PB162. Preto kód obsahuje 2 chyby - opravte ich. Reviewer kódu si na papier zapísal:
* V hlavičke triedy chýba gnewfryckýw tzgp [2 nečitateľné slová]
* Chýba metóda, ktorá tam má byť, keďže je tam... [reviewer bol vyrušený meetingom a nedopísal poznámky]

<br />

Vytvorte triedu `Warehouse`, ktorá bude reprezentovať veľkosklad.
* Základné dátové štruktúry:
    - Sklad si pamätá pre každú položku počet kusov na sklade.
    - Sklad si tiež pamätá všetkých svojich dodávateľov.
    - V atribútoch je zakázané použiť List.
    - Položky sú považované za rovnaké, ak majú rovnaký názov.
    - Dodávatelia sú považovaní za rovnakých, ak majú rovnaké ID.
* Pri vytvorení sklad berie ako parameter kolekciu svojich dodávateľov. Ak je zoznam dodávateľov null, vyhoďte vhodnú výnimku. Existuje však tiež spôsob, ako sklad vytvoriť bez akejkoľvek špecifikácie dodávateľov, pričom v takom prípade bude kolekcia dodávateľov zo začiatku prázdna.
* Metóda `registerSupplier(Supplier supplier)` pridá dodávateľa skladu, ak supplier nie je null. Vráti false, ak je supplier null alebo ak supplier už bol v sklade registrovaný. Inak vráti true.
* Metóda `getSortedSuppliers()` vráti utriedenú množinu všetkých dodávateľov. Dodávatelia budú utriedení podľa ID.
* Metóda `addSupplies(Supplier supplier, Item item, int count)` pridá na sklad count položiek item, ktoré doviezol supplier.
    - Vyhoďte vhodnú výnimku, ak supplier nie je medzi registrovanými dodávateľmi v tomto sklade.
* Metóda `getItems()` vráti vhodnú kolekciu bez duplicít, ktorá bude obsahovať všetky položky na sklade (bez ich počtu).
* Metóda `printItemDetails(Item item)` vypíše názov a cenu položky zo vstupu.
* Metóda `countExpensiveItems()` vráti koľko kusov drahých položiek je spolu na sklade. Sklad považuje za drahé tie položky, ktoré stoja aspoň 150. Použite jediný stream.
* Neverejná metóda `findItemByName(String name)` nájde podľa názvu item v sklade. Ak ho nenájde, vráti null. Použite jediný stream.
* Metóda `writeItems(OutputStream os)` vypíše bez duplicít položky utriedené podľa ceny a v prípade zhody podľa názvu, pričom ku každej položke vypíše počet kusov na sklade. Formát výpisu je "%NÁZOV% (cena: %CENA%): %KUSOV_NA_SKLADE%", napríklad "BANANY (cena: 99): 18". Každá položka je oddelená novým riadkom. Na získanie takýchto položiek použite jediný stream.
* Metóda `enoughSupplies(File file)` prečíta zo súboru požadované položky a ich cenu vo formáte "ITEM_NAME ITEM_COUNT", pričom každý dotaz je na novom riadku. Metóda vráti true, ak sa dá všetkým položkám vyhovieť, false inak. Jedna položka bude na zozname najviac raz. Pri I/O chybe alebo nesprávnom formáte vstupu metóda vyhodí SuppliesQueryFailedException.
* Metóda `enoughSupplies(InputStream is)` prečíta zo vstupného streamu požiadavky vypísané vyššie a spraví to isté. Pri I/O chybe alebo nesprávnom formáte vstupu metóda taktiež vyhodí SuppliesQueryFailedException.


## Kontrolný výpis:
```
Registering already registered supplier: false
Sorted suppliers: [Hyza (ID 1), XXL Supplier (ID 2), Ecco (ID 3)]
Items (the order does not matter): [Computer (cena: 2000), Drone (cena: 2000), Pork (cena: 15), Coat (cena: 180), Fridge (cena: 4500)]

Details about a computer:
Computer (cena: 2000)
Expensive items: 1165

Items sorted by price (reversed order is also ok):
Pork (cena: 15): 500
Coat (cena: 180): 100
Computer (cena: 2000): 1000
Drone (cena: 2000): 50
Fridge (cena: 4500): 15

enoughSupplies(input_enough): true
enoughSupplies(input_not_enough): false
```