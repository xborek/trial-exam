package cz.muni.fi.pb162.xjudiny.trialexam;

/**
 * This Exception can be thrown when a query for supplies failed.
 *
 * @author Jakub Judiny
 */
public class SuppliesQueryFailedException extends Exception {
    public SuppliesQueryFailedException() {
        super();
    }

    public SuppliesQueryFailedException(String message) {
        super(message);
    }

    public SuppliesQueryFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public SuppliesQueryFailedException(Throwable cause) {
        super(cause);
    }
}
