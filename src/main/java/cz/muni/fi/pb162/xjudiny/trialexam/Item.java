package cz.muni.fi.pb162.xjudiny.trialexam;

import java.util.Objects;

/**
 * Represents an item. For example a juice.
 *
 * @author Jakub Judiny
 */
public class Item {
    private final String name;
    private final int price;

    /**
     * Constructor.
     */
    public Item(String name, int price) {
        Objects.requireNonNull(name);
        this.name = name;
        this.price = price;

        if (price <= 0){
            throw new IllegalArgumentException("price must be > 0");
        }
    }

    public int getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(name, item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
