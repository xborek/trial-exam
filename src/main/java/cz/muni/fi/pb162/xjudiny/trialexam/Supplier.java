package cz.muni.fi.pb162.xjudiny.trialexam;

import java.util.Objects;

/**
 * Represents a supplier for a warehouse.
 *
 * @author Janko Vyhodený
 */
public class Supplier implements Comparable<Supplier>{
    private final int id;
    private final String name;

    /**
     * Constructor.
     */
    public Supplier(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name + " (ID " + id + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Supplier supplier)) return false;
        return id == supplier.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int compareTo(Supplier o) {
        //Supplier s = (Supplier) o;
        return this.id - o.id;
    }
}
