package cz.muni.fi.pb162.xjudiny.trialexam;

import java.io.File;
import java.io.IOException;
import java.util.Set;

/**
 * @author Jakub Judiny
 */
public class Demo {
    public static void main(String[] args) throws IOException, SuppliesQueryFailedException {
        try {
            Item item = new Item("Invalid", -1);
            throw new RuntimeException("Creating item with negative or zero price should fail.");
        } catch (RuntimeException e) {
        }

        Supplier supplier1 = new Supplier(1, "Hyza");
        Supplier supplier2 = new Supplier(2, "XXL Supplier");
        Supplier supplier3 = new Supplier(3, "Ecco");
        Supplier supplier4 = new Supplier(3, "Ozeta");
        Supplier supplier5 = new Supplier(5, "Dell");

        Warehouse warehouse0 = new Warehouse();

        Warehouse warehouse = new Warehouse(Set.of(supplier1, supplier2, supplier3));
        System.out.println("Registering already registered supplier: " + warehouse.registerSupplier(supplier4));
        warehouse.registerSupplier(null);

        System.out.println("Sorted suppliers: " + warehouse.getSortedSuppliers());

        // Add items.
        try {
            warehouse.addSupplies(supplier5, new Item("aaa", 1), 1);
            throw new RuntimeException("Calling addSupplies with unregistered supplier should fail.");
        } catch (RuntimeException e) {
        }

        Item computer = new Item("Computer", 2000);
        warehouse.addSupplies(supplier1, new Item("Pork", 15), 500);
        warehouse.addSupplies(supplier2, new Item("Coat", 180), 100);
        warehouse.addSupplies(supplier3, new Item("Drone", 2000), 50);
        warehouse.addSupplies(supplier3, computer, 500);
        warehouse.addSupplies(supplier3, computer, 500);
        warehouse.addSupplies(supplier3, new Item("Fridge", 4500), 15);

        System.out.println("Items (the order does not matter): " + warehouse.getItems());
        System.out.println();

        System.out.println("Details about a computer:");
        warehouse.printItemDetails(computer);
        System.out.println("Expensive items: " + warehouse.countExpensiveItems());

        System.out.println();
        System.out.println("Items sorted by price (reversed order is also ok):");
        warehouse.writeItems(System.out);
        System.out.println();

        System.out.println("enoughSupplies(input_enough): " + warehouse.enoughSupplies(new File("input_enough.txt")));
        System.out.println("enoughSupplies(input_not_enough): " + warehouse.enoughSupplies(new File("input_not_enough.txt")));
    }
}