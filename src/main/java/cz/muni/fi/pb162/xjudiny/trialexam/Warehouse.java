package cz.muni.fi.pb162.xjudiny.trialexam;

import javax.sound.sampled.Line;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.cert.TrustAnchor;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author Vladimir Borek
 */
public class Warehouse {

    private static final int ITEM_NAME_INDEX = 0;
    private static final int ITEM_COUNT_INDEX = 1;
    private static final int SUPPLIES_LINE_LENGTH = 2;
    private static final String SUPPLIES_LINE_SPLITTER = " ";
    private static final int EXPENSIVE_ITEM_PRICE = 150;

    private final Map<Item, Integer> items = new HashMap<>();
    private final Set<Supplier> suppliers;

    public Warehouse(Collection<Supplier> suppliersCollection) {
        Objects.requireNonNull(suppliersCollection, "Supplier Collection cannot be null");
        this.suppliers = new HashSet<>(suppliersCollection);
    }

    public Warehouse() {
        this(new HashSet<>());
    }

    public boolean registerSupplier(Supplier supplier) {
        if (supplier == null || this.suppliers.contains(supplier)) {
            return false;
        }

        this.suppliers.add(supplier);
        return true;
    }

    public SortedSet<Supplier> getSortedSuppliers() {
        return Collections.unmodifiableSortedSet(new TreeSet<>(this.suppliers));
    }

    public void addSupplies(Supplier supplier, Item item, int count) {
        if (!this.suppliers.contains(supplier)) {
            throw new IllegalArgumentException("Not registered supplier");
        }

        this.items.put(item, this.items.getOrDefault(item, 0) + count);
    }

    public Set<Item> getItems() {
        return Collections.unmodifiableSet(items.keySet());
    }

    public void printItemDetails(Item item) {
        System.out.println(item);
    }

    public long countExpensiveItems() {
        return items.keySet().stream()
                .filter(item -> item.getPrice() >= EXPENSIVE_ITEM_PRICE)
                .count();
    }

    private Item findItemByName(String name) {
        return items.keySet().stream()
                .filter(item -> Objects.equals(item.getName(), name))
                .findFirst()
                .orElse(null);
    }

    public void writeItems(OutputStream os) throws IOException {
        var itemsToPrint = items.entrySet().stream()
                .sorted(new Comparator<Map.Entry<Item, Integer>>() {
                    @Override
                    public int compare(Map.Entry<Item, Integer> o1, Map.Entry<Item, Integer> o2) {
                        int firstCompare = o1.getKey().getPrice() - o2.getKey().getPrice();

                        if (firstCompare == 0) {
                            return o1.getKey().getName().compareTo(o2.getKey().getName());
                        }
                        return firstCompare;
                    }
                })
                .toList();


        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os));

        for (var entry : itemsToPrint) {
            bufferedWriter.write(String.format("%s (cena: %d): %d", entry.getKey().getName(), entry.getKey().getPrice(), entry.getValue()));
            bufferedWriter.newLine();
        }
        bufferedWriter.flush();
    }

    public boolean enoughSupplies(File file) throws SuppliesQueryFailedException {
        try (FileInputStream is = new FileInputStream(file)) {
            return enoughSupplies(is);

        } catch (IOException e) {
            throw new SuppliesQueryFailedException("error when working with file ", e);
        }
    }

    public boolean enoughSupplies(InputStream is) throws SuppliesQueryFailedException {

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                var lineSplit = line.split(SUPPLIES_LINE_SPLITTER);

                if (lineSplit.length != SUPPLIES_LINE_LENGTH) {
                    throw new SuppliesQueryFailedException("Invalid format");
                }

                String item_name = lineSplit[ITEM_NAME_INDEX];
                int item_count = Integer.parseInt(lineSplit[ITEM_COUNT_INDEX]);

                Item item = findItemByName(item_name);

                if (items.getOrDefault(item, 0) < item_count) {
                    return false;
                }
            }
            return true;

        } catch (IOException e) {
            throw new SuppliesQueryFailedException("Error when reading file", e);
        }
    }
}
